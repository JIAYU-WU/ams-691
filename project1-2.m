function y=inner(a,A,b)
%Project1_problem#2
%Author: JIAYU WU
%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%
%inputs:
%a and b are vectors
%A is a matrix
%%%%%%%%%%%%%%%%%%
%output:
%y=a'*A*b

%Get the sizes of our variables
n1=max(size(a)); n2=max(size(b));
[m n]=size(A);

%Check if the inner product is well-defined
assert(n1==m,'Dimension is not matched');
assert(n==n2,'Dimension is not matched');

%First matrix-vector
tmp=zeros(m,1);
for i=1:m
    temp=0;
    for j=1:n
        temp=temp+A(i,j)*b(j);
    end
    tmp(i)=temp;
end

%Then vector-vector
y=0;
for ii=1:m
    y=y+a(ii)*tmp(ii);
end
    
end