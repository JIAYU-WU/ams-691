# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 13:00:39 2016

@author: wujiayu
"""

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Homework on Control Flow and Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(100 points total)\n",
    "\n",
    "In this homework, we use Jupyter Notebook to practice with the control flows and functions in Python. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Conditional Statements\n",
    "\n",
    "(15 points)\n",
    "Translate the following MATLAB code into Python using `if-elif-else`. \n",
    "```\n",
    "n = input('Enter a number: ');\n",
    "\n",
    "switch n\n",
    "    case -1\n",
    "        disp('negative one')\n",
    "    case 0\n",
    "        disp('zero')\n",
    "    case 1\n",
    "        disp('positive one')\n",
    "    otherwise\n",
    "        disp('other value')\n",
    "end\n",
    "```\n",
    "\n",
    "Note that the input command can be literally translated to\n",
    "```\n",
    "n = int(input('Enter a number: '))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "n = int(input('Enter a number: '))\n",
    "if n==-1:\n",
    "    print (\"negative one\")\n",
    "elif n==0:\n",
    "    print(\"zero\")\n",
    "elif n==1:\n",
    "    print(\"positive one\")\n",
    "else:\n",
    "    print(\"other value\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Enter a number: -1\n",
    "negative one\n",
    "Enter a number: 2\n",
    "other value\n",
    "Enter a number: 0\n",
    "zero"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. While Loops\n",
    "\n",
    "(15 points) Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:\n",
    "\n",
    "```1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...```\n",
    "\n",
    "Write a while-loop to construct a list of the numbers in the Fibonacci sequence whose values do not exceed 4,000,000."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "a,b=0,1\n",
    "while b<4000000:\n",
    "    print(b)\n",
    "    a,b=b,a+b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1\n",
    "1\n",
    "2\n",
    "3\n",
    "5\n",
    "8\n",
    "13\n",
    "21\n",
    "34\n",
    "55\n",
    "89\n",
    "144\n",
    "233\n",
    "377\n",
    "610\n",
    "987\n",
    "1597\n",
    "2584\n",
    "4181\n",
    "6765\n",
    "10946\n",
    "17711\n",
    "28657\n",
    "46368\n",
    "75025\n",
    "121393\n",
    "196418\n",
    "317811\n",
    "514229\n",
    "832040\n",
    "1346269\n",
    "2178309\n",
    "3524578"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. List Comprehension\n",
    "\n",
    "(20 points) A palindrome is a word, phrase, number, or other sequence of characters which reads the same backward or forward. \n",
    "\n",
    "Use list comprehension to generate a list of palindrome made from the product of 3 digit numbers. \n",
    "\n",
    "Hint: if `x` and `y` are two integers, you can use `str(x*y) == str(y*x)[::-1]` to determine whether `x * y` is a palindrome."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "[x*y for x in range(100,999,1) for y in range(100,999,1) if str(x*y) == str(y*x)[::-1]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " 29392,\n",
    " 33233,\n",
    " 66466,\n",
    " 70307,\n",
    " 99699,\n",
    " 48384,\n",
    " 63336,\n",
    " 65856,\n",
    " 80808,\n",
    " 53235,\n",
    " 59995,\n",
    " 61516,\n",
    " 98189,\n",
    " 17271,\n",
    " 18981,\n",
    " 43434,\n",
    " 85158,\n",
    " 86868,\n",
    " 31313,\n",
    " 46364,\n",
    " 62626,\n",
    " 77677,\n",
    " 93939,\n",
    " 165561,\n",
    " 25752,\n",
    " 44544,\n",
    " 63336,\n",
    " 82128,\n",
    " 52325,\n",
    " 57575,\n",
    " 23232,\n",
    " 29392,\n",
    " 40304,\n",
    " 46464,\n",
    " 63536,\n",
    " 69696,\n",
    " 80608,\n",
    " 86768,\n",
    " 55755,\n",
    " 59295,\n",
    " 82128,\n",
    " 98589,\n",
    " 24742,\n",
    " 29192,\n",
    " 44144,\n",
    " 68886,\n",
    " 83838,\n",
    " 88288,\n",
    " 28282,\n",
    " 51015,\n",
    " 65156,\n",
    " 79297,\n",
    " 80908,\n",
    " 155551,\n",
    " 18281,\n",
    " 31313,\n",
    " 49594,\n",
    " 62626,\n",
    " 79097,\n",
    " 92129,\n",
    " 93939,\n",
    " 20202,\n",
    " 21112,\n",
    " 22022,\n",
    " 40404,\n",
    " 41314,\n",
    " 42224,\n",
    " 43134,\n",
    " 44044,\n",
    " 60606,\n",
    " 61516,\n",
    " 62426,\n",
    " 63336,\n",
    " 64246,\n",
    " 65156,\n",
    " 66066,\n",
    " 80808,\n",
    " 81718,\n",
    " 82628,\n",
    " 83538,\n",
    " 84448,\n",
    " 85358,\n",
    " 86268,\n",
    " 87178,\n",
    " 88088,\n",
    " 28182,\n",
    " 74847,\n",
    " 81618,\n",
    " 21712,\n",
    " 29992,\n",
    " 44344,\n",
    " 80408,\n",
    " 88688,\n",
    " 50505,\n",
    " 51615,\n",
    " 52725,\n",
    " 53835,\n",
    " 54945,\n",
    " 87978,\n",
    " 41514,\n",
    " 45254,\n",
    " 86768,\n",
    " 127721,\n",
    " 133331,\n",
    " 168861,\n",
    " 174471,\n",
    " 180081,\n",
    " 27072,\n",
    " 29892,\n",
    " 44744,\n",
    " 86668,\n",
    " 27972,\n",
    " 48384,\n",
    " 55755,\n",
    " 76167,\n",
    " 83538,\n",
    " 90909,\n",
    " 19291,\n",
    " 36863,\n",
    " 52525,\n",
    " 87478,\n",
    " 21312,\n",
    " 23232,\n",
    " 25152,\n",
    " 27072,\n",
    " 40704,\n",
    " 42624,\n",
    " 44544,\n",
    " 46464,\n",
    " 48384,\n",
    " 63936,\n",
    " 65856,\n",
    " 67776,\n",
    " 69696,\n",
    " 23932,\n",
    " 36863,\n",
    " 49794,\n",
    " 71217,\n",
    " 84148,\n",
    " 97079,\n",
    " 142241,\n",
    " 50505,\n",
    " 53235,\n",
    " 29792,\n",
    " 42924,\n",
    " 65856,\n",
    " 88788,\n",
    " 43734,\n",
    " 47674,\n",
    " 71117,\n",
    " 75057,\n",
    " 64746,\n",
    " 69696,\n",
    " 84348,\n",
    " 89298,\n",
    " 33233,\n",
    " 46964,\n",
    " 66466,\n",
    " 99699,\n",
    " 20502,\n",
    " 59295,\n",
    " 79797,\n",
    " 98289,\n",
    " 20402,\n",
    " 21412,\n",
    " 22422,\n",
    " 23432,\n",
    " 24442,\n",
    " 25452,\n",
    " 26462,\n",
    " 27472,\n",
    " 28482,\n",
    " 29492,\n",
    " 40804,\n",
    " 41814,\n",
    " 42824,\n",
    " 43834,\n",
    " 44844,\n",
    " 45854,\n",
    " 46864,\n",
    " 47874,\n",
    " 48884,\n",
    " 49894,\n",
    " 21112,\n",
    " 42224,\n",
    " 63336,\n",
    " 84448,\n",
    " 21012,\n",
    " 42024,\n",
    " 63036,\n",
    " 84048,\n",
    " 55555,\n",
    " 21012,\n",
    " 26162,\n",
    " 42024,\n",
    " 47174,\n",
    " 63036,\n",
    " 68186,\n",
    " 84048,\n",
    " 89198,\n",
    " 41814,\n",
    " 45954,\n",
    " 92529,\n",
    " 96669,\n",
    " 27872,\n",
    " 42224,\n",
    " 84448,\n",
    " 57475,\n",
    " 63536,\n",
    " 102201,\n",
    " 137731,\n",
    " 154451,\n",
    " 171171,\n",
    " 189981,\n",
    " 204402,\n",
    " 23632,\n",
    " 45154,\n",
    " 68786,\n",
    " 92629,\n",
    " 21412,\n",
    " 23532,\n",
    " 25652,\n",
    " 27772,\n",
    " 29892,\n",
    " 40704,\n",
    " 42824,\n",
    " 44944,\n",
    " 68586,\n",
    " 76467,\n",
    " 84348,\n",
    " 92229,\n",
    " 25252,\n",
    " 45154,\n",
    " 65056,\n",
    " 51815,\n",
    " 53535,\n",
    " 55255,\n",
    " 29592,\n",
    " 48384,\n",
    " 63936,\n",
    " 67176,\n",
    " 82728,\n",
    " 43834,\n",
    " 76167,\n",
    " 64746,\n",
    " 84148,\n",
    " 89598,\n",
    " 30003,\n",
    " 42924,\n",
    " 49494,\n",
    " 60006,\n",
    " 72927,\n",
    " 79497,\n",
    " 90009,\n",
    " 26962,\n",
    " 54145,\n",
    " 83538,\n",
    " 22422,\n",
    " 23532,\n",
    " 24642,\n",
    " 25752,\n",
    " 26862,\n",
    " 27972,\n",
    " 40404,\n",
    " 41514,\n",
    " 42624,\n",
    " 43734,\n",
    " 44844,\n",
    " 45954,\n",
    " 60606,\n",
    " 61716,\n",
    " 62826,\n",
    " 63936,\n",
    " 80808,\n",
    " 81918,\n",
    " 210012,\n",
    " 56865,\n",
    " 59095,\n",
    " 81618,\n",
    " 98789,\n",
    " 188881,\n",
    " 29792,\n",
    " 48384,\n",
    " 65856,\n",
    " 84448,\n",
    " 52425,\n",
    " 57375,\n",
    " 27572,\n",
    " 49494,\n",
    " 80908,\n",
    " 41314,\n",
    " 45854,\n",
    " 82628,\n",
    " 99199,\n",
    " 182281,\n",
    " 69996,\n",
    " 86868,\n",
    " 35953,\n",
    " 40304,\n",
    " 47174,\n",
    " 80608,\n",
    " 87478,\n",
    " 28182,\n",
    " 33033,\n",
    " 66066,\n",
    " 99099,\n",
    " 108801,\n",
    " 111111,\n",
    " 138831,\n",
    " 141141,\n",
    " 168861,\n",
    " 171171,\n",
    " 198891,\n",
    " 219912,\n",
    " 222222,\n",
    " 23432,\n",
    " 25752,\n",
    " 42224,\n",
    " 44544,\n",
    " 46864,\n",
    " 61016,\n",
    " 63336,\n",
    " 65656,\n",
    " 67976,\n",
    " 82128,\n",
    " 84448,\n",
    " 86768,\n",
    " 26562,\n",
    " 52425,\n",
    " 78987,\n",
    " 28782,\n",
    " 60606,\n",
    " 83538,\n",
    " 56165,\n",
    " 57575,\n",
    " 58985,\n",
    " 25252,\n",
    " 46964,\n",
    " 82128,\n",
    " 43134,\n",
    " 47874,\n",
    " 86268,\n",
    " 91719,\n",
    " 44744,\n",
    " 69496,\n",
    " 83538,\n",
    " 219912,\n",
    " 56165,\n",
    " 60706,\n",
    " 67876,\n",
    " 128821,\n",
    " 44344,\n",
    " 51815,\n",
    " 88688,\n",
    " 24442,\n",
    " 25652,\n",
    " 26862,\n",
    " 44044,\n",
    " 45254,\n",
    " 46464,\n",
    " 47674,\n",
    " 48884,\n",
    " 66066,\n",
    " 67276,\n",
    " 68486,\n",
    " 69696,\n",
    " 88088,\n",
    " 89298,\n",
    " 201102,\n",
    " 214412,\n",
    " 227722,\n",
    " 27572,\n",
    " 40504,\n",
    " 69296,\n",
    " 82228,\n",
    " 54145,\n",
    " 57575,\n",
    " 28782,\n",
    " 66666,\n",
    " 81918,\n",
    " 89298,\n",
    " 49894,\n",
    " 74347,\n",
    " 171171,\n",
    " 44144,\n",
    " 63736,\n",
    " 88288,\n",
    " 215512,\n",
    " 39093,\n",
    " 53535,\n",
    " 85158,\n",
    " 58985,\n",
    " 66766,\n",
    " 74547,\n",
    " 82328,\n",
    " 90109,\n",
    " 25452,\n",
    " 27972,\n",
    " 48384,\n",
    " 105501,\n",
    " 133331,\n",
    " 161161,\n",
    " 178871,\n",
    " 221122,\n",
    " 238832,\n",
    " 26162,\n",
    " 43434,\n",
    " 60706,\n",
    " 69596,\n",
    " 86868,\n",
    " 56865,\n",
    " 57375,\n",
    " 40704,\n",
    " 44544,\n",
    " 48384,\n",
    " 55255,\n",
    " 63736,\n",
    " 49794,\n",
    " 68886,\n",
    " 87978,\n",
    " 210012,\n",
    " 27972,\n",
    " 30303,\n",
    " 40404,\n",
    " 50505,\n",
    " 60606,\n",
    " 70707,\n",
    " 80808,\n",
    " 90909,\n",
    " 111111,\n",
    " 222222,\n",
    " 117711,\n",
    " 26462,\n",
    " 27772,\n",
    " 61016,\n",
    " 90209,\n",
    " 46464,\n",
    " 69696,\n",
    " 213312,\n",
    " 219912,\n",
    " 234432,\n",
    " 255552,\n",
    " 59095,\n",
    " 29792,\n",
    " 66766,\n",
    " 239932,\n",
    " 34443,\n",
    " 68886,\n",
    " 83838,\n",
    " 27872,\n",
    " 46364,\n",
    " 73437,\n",
    " 99799,\n",
    " 33333,\n",
    " 44444,\n",
    " 55555,\n",
    " 66666,\n",
    " 77777,\n",
    " 88888,\n",
    " 99999,\n",
    " 122221,\n",
    " 244442,\n",
    " 27472,\n",
    " 84048,\n",
    " 86768,\n",
    " 30303,\n",
    " 33033,\n",
    " 40404,\n",
    " 43134,\n",
    " 50505,\n",
    " 53235,\n",
    " 60606,\n",
    " 63336,\n",
    " 66066,\n",
    " 70707,\n",
    " 73437,\n",
    " 76167,\n",
    " 80808,\n",
    " 83538,\n",
    " 86268,\n",
    " 90909,\n",
    " 93639,\n",
    " 96369,\n",
    " 99099,\n",
    " 111111,\n",
    " 141141,\n",
    " 171171,\n",
    " 222222,\n",
    " 252252,\n",
    " 29592,\n",
    " 40004,\n",
    " 49594,\n",
    " 60006,\n",
    " 69596,\n",
    " 80008,\n",
    " 89598,\n",
    " 220022,\n",
    " ...]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Functions\n",
    "\n",
    "2a. (10 points) Write a function that determines if a number is prime."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def isprime(n):\n",
    "    def isprime(n):\n",
    "    for i in range(3,n):\n",
    "        if n%i==0:\n",
    "            return False\n",
    "    return True"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# test codes\n",
    "print(isprime(2))\n",
    "print(isprime(10))\n",
    "print(isprime(17))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "True\n",
    "False\n",
    "True"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "2b. (10 points) Write a function that finds all the factors of a number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def factorize(n):\n",
    "    answer=[]\n",
    "    for i in range(1, n + 1):\n",
    "        if n % i == 0:\n",
    "            answer.append(i)\n",
    "    return answer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# test codes\n",
    "print(factorize(2))\n",
    "print(factorize(72))\n",
    "print(factorize(196))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[1, 2]\n",
    "[1, 2, 3, 4, 6, 8, 9, 12, 18, 24, 36, 72]\n",
    "[1, 2, 4, 7, 14, 28, 49, 98, 196]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2c. (10 points) Write a function that finds the prime factorization of a number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def prime_factorize(n):\n",
    "    answer = []\n",
    "    i = 2\n",
    "    while i * i <= n:\n",
    "        if n % i:\n",
    "            i += 1\n",
    "        else:\n",
    "            n //= i\n",
    "            answer.append(i)\n",
    "    if n > 1:\n",
    "        answer.append(n)\n",
    "    return answer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# test codes\n",
    "print(prime_factorize(2))\n",
    "print(prime_factorize(72))\n",
    "print(prime_factorize(196))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[2]\n",
    "[2, 2, 2, 3, 3]\n",
    "[2, 2, 7, 7]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Recursive Functions\n",
    "(20 points) The following function uses recursion to generate the $n$th row of Pascal's triangle:\n",
    "```\n",
    "              1\n",
    "           1     1\n",
    "        1     2     1\n",
    "      1    3     3     1\n",
    "   1    4     6     4    1\n",
    "1     5    10    10    5    1\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def pascal(n):\n",
    "    if n == 1:\n",
    "        return [1]\n",
    "    else:\n",
    "        p_line = pascal(n-1)\n",
    "        line = [ p_line[i]+p_line[i+1] for i in range(len(p_line)-1)]\n",
    "        line.insert(0,1)\n",
    "        line.append(1)\n",
    "    return line\n",
    "\n",
    "print(pascal(6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rewrite the above function to use iteration (using only `while` or `for` loops) instead of recursion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import math               \n",
    "def pascal(rows):\n",
    "    result=[]\n",
    "    for count in range(rows):\n",
    "        row=[]\n",
    "        for element in range(count + 1): \n",
    "        row.append(int((math.factorial(count))/((math.factorial(element))*math.factorial(count-element))))\n",
    "        result.append(row)\n",
    "return result\n",
    "\n",
    "print(pascal(6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[[1], [1, 1], [1, 2, 1], [1, 3, 3, 1], [1, 4, 6, 4, 1], [1, 5, 10, 10, 5, 1]]"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Root]",
   "language": "python",
   "name": "Python [Root]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}