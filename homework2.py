# -*- coding: utf-8 -*-
"""
Created on Sat Oct 29 21:46:16 2016

@author: wujiayu
"""
'''
Markov Chain
'''

import numpy as num

num.random.seed(1993)

n_states = 5
n_steps = 50
tolerance = 1e-5

# Random transition matrix 
P = num.random.rand(n_states, n_states)
p = num.random.rand(n_states)

# Normalize the row of P
P /= P.sum(axis=1)[:,num.newaxis]

# Normalize p
p /= p.sum()

# Take steps
for k in range(n_steps):
    p = P.T.dot(p)

p_50 = p
print (p_50)

# Compute stationary state
w, v = num.linalg.eig(P.T)

j_stationary = num.argmin(abs(w - 1.0))
p_stationary = v[:,j_stationary].real
p_stationary /= p_stationary.sum()
print (p_stationary)

# Check
if all(abs(p_50 - p_stationary) < tolerance):
    print ("Tolerance is in infinity norm")

if num.linalg.norm(p_50 - p_stationary) < tolerance:
    print ("Tolerance is in 2-norm")