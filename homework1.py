# -*- coding: utf-8 -*-
"""
Created on Sat Oct 29 21:06:43 2016

@author: wujiayu
"""
'''
#1.Mandelbrot Set
'''
import numpy as num
import matplotlib.pyplot as plt
from numpy import newaxis
def compute_mandelbrot(N_max, some_threshold, nx, ny):
    # A grid of c-values
    x=num.linspace(-2,1,nx)
    y=num.linspace(-1.5,1.5,ny)
    c=x[:,newaxis]+1j*y[newaxis,:]


    #Mandelbrot iteration
    z=c
    for j in range(N_max):
        z=z**2+c
    
    mandelbrot_set=(abs(z)< some_threshold)
    
    return mandelbrot_set

    
#Save 
mandelbrot_set=compute_mandelbrot(50,50.,1000,1000)
plt.imshow(mandelbrot_set.T, extent=[-2, 1, -1.5, 1.5])
plt.gray()
plt.savefig('mandelbrot.png')